import React, { useState, useEffect } from 'react';
// import ReactDOM from 'react-dom';
import { Form, Input, Button, Checkbox, Alert } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { authenticate } from '../../store/actions/auth';
import authSelector from '../../store/selectors/auth';
import Link from 'next/link';
import Router from "next/router";
import { isEmpty } from 'lodash';

// component
import Head from '../../components/head';

const defaultResponse:any = {
  status: null,
  massage: '',
}

const LoginForm = () => {
  const dispatch:any = useDispatch();
  const auth = useSelector(authSelector);
  const [loading, setLoading] = useState<boolean>(false);
  const [responseSubmit, setResponseSubmit] = useState<any>(defaultResponse);

  const checkExistLogin = async () => {
    if (!isEmpty(auth?.data)) {
      Router.push("/article");
    }
    return false;
  }

  const _onSuccess = () => {
    setLoading(false);
  };

  const _onError = (_data: any) => {
    setResponseSubmit(_data);
    setLoading(false);
  };

  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
    setLoading(true);
    dispatch(
      authenticate(
        values,
        _onSuccess,
        _onError,
      ),
    );
  };

  useEffect(() => {
    checkExistLogin();
  }, [auth]);

  return (
    <>
      <Head />
      <div className="container">
        <div className="login-container mt-5">
          <h2 className="mb-3 mt-3 text-center">
            Halaman Login
          </h2>
          {responseSubmit?.status && (
            <Alert
                // message={showResponseLabel?.[responseSubmit?.status]}
                description={responseSubmit?.message}
                type={responseSubmit?.status || 'info'}
                showIcon
                closable
            />
          )}
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input your Username!',
                },
              ]}
            >
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>
            <Form.Item
              name="password"
              // label="Password"
              rules={[
                {
                  required: true,
                  message: 'Please input your Password!',
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="">
                Forgot password
              </a>
            </Form.Item>

            <Form.Item className="text-center">
              <Button type="primary" htmlType="submit" className="login-form-button mb-3" loading={loading} disabled={loading}>
                Login
              </Button>
              <Link href="/register">
                Daftar Sekarang
              </Link>
            </Form.Item>
          </Form>
        </div>
      </div>
    </>
  );
};

export default LoginForm;

// ReactDOM.render(<LoginForm />, document.getElementById('container'));