import React, { useState, useEffect } from 'react';
import { 
    Table, 
    Row, 
    Col, 
    Pagination, 
    Button, 
    Space, 
    Alert,
    Input,
    DatePicker,
} from 'antd';
import type { ColumnsType, TableProps } from 'antd/es/table';
import type { PaginationProps } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useIndexedDB } from 'react-indexed-db';
import axios from 'axios';
import Router from "next/router";
// import Link from 'next/link';
import { PlusCircleOutlined, LogoutOutlined, SyncOutlined } from '@ant-design/icons'
import { isEmpty } from 'lodash';
import { authClearData } from '../../store/actions/auth';
import authSelector from '../../store/selectors/auth';
import Moment from 'react-moment';
import 'moment-timezone';

// component
import Head from '../../components/head';
import Form from './form';

interface DataType {
    key: React.Key;
    no: string;
    title: string;
    createdAt: any;
    status: any;
    action: any;
}

Moment.globalTimezone = 'Asia/Jakarta';
Moment.globalLocal = true;
const { Search } = Input;

const Article = () => {
    const dispatch: any = useDispatch();
    const auth = useSelector(authSelector);

    const [loading, setLoading] = useState<boolean>(false);
    const [form, setForm] = useState<boolean>(false);
    const [data, setData] = useState<any[]>([]);
    const [search, setSearch] = useState<string>('');
    const [filterDate, setFilterDate] = useState<string[]>([]);
    const [dataItem, setDataItem] = useState<any>({});
    const [metadata, setMetadata] = useState<any>({
        pageSize: 10,
        page: 1,
        total: 0,
    });
    const [responseRequest, setResponseRequest] = useState<any>({
        status: null,
        massage: '',
    });

    const checkExistLogin = async () => {
        if (isEmpty(auth?.data)) {
        Router.push("/");
        }
        return false;
    }

    const _onFetchData = async (pageSize: number = 10, page: number = 1, sorter: any = {}) => {
        setLoading(true);
        try {
            let url = 'https://fe-technical-test.herokuapp.com/api/articles';
            url += `?pagination[pageSize]=${pageSize}`;
            url += `&pagination[page]=${page}`;
            
            if (!isEmpty(sorter)) {
                url += `&filters[${sorter?.field}][$orderBy]=${sorter?.order || 'ascend'}`;
            }

            if (search) {
                url += `&filters[title][$contains]=${search}`;
            }

            if (!isEmpty(filterDate)) {
                url += `&filters[createdAt][$startsWith]=${filterDate?.[0]}`;
                url += `&filters[createdAt][$endsWith]=${filterDate?.[1]}`;
            }

            const config = {
                headers: { 
                    'Authorization': `Bearer ${auth?.data?.token}` 
                },
            };

            const response: any = await axios.get(url, config);
            if (response?.status === 200) {
                const _data: any[] = [];

                response?.data?.data.map((_value: any) => {
                    _data.push({
                        id: _value?.id,
                        ..._value?.attributes,
                    })
                })

                setMetadata(response?.data?.meta?.pagination);
                setData(_data);
                setLoading(false);
            } else {
                throw Error();
            }
        } catch (error: any) {
            console.log('errors', error);
            setData([]);
            setLoading(false);
        }
    };

    const onSave = async (values: any, id: number = 0) => {
        setLoading(true);

        try {
            const config = {
                headers: { 'Authorization': `Bearer ${auth?.data?.token}` }
            };

            let url = 'https://fe-technical-test.herokuapp.com/api/articles';
            let response = null;
            
            const data = {
                data: {
                    title: values?.title,
                    content: values?.content,
                }
            };
            
            if (id) {
                url += `/${id}`;

                response = await axios.put(url, data, config);
            } else {
                response = await axios.post(url, data, config);
            }

            if (response?.status === 200) {
                _onFetchData(metadata?.pageSize, metadata?.page);
                setResponseRequest({
                    status: 'success',
                    message: `Berhasil ${ id ? 'mengubah' : 'menambah' } artikel ${values?.title}`,
                });
            } else {
                throw Error();
            }
        } catch (error: any) {
            if (error?.response?.status === 400) {
                setResponseRequest({
                    status: 'warning',
                    message: error?.response?.data?.error?.message,
                });
            } else {
                setResponseRequest({
                    status: 'error',
                    message: `Gagal ${ id ? 'mengubah' : 'menambah' } artikel ${values?.title}`,
                });
            }
        }

        setLoading(false);
    };

    const onDelete = async (_values: any) => {
        setLoading(true);

        try {
            const url = `https://fe-technical-test.herokuapp.com/api/articles/${_values?.id}`;
            const config = {
                headers: { 'Authorization': `Bearer ${auth?.data?.token}` }
            };
            const response = await axios.delete(url, config);

            if (response?.status === 200) {
                _onFetchData(metadata?.pageSize, metadata?.page);
                setResponseRequest({
                    status: 'success',
                    message: `Berhasil menghapus artikel ${_values?.title}`,
                });
            } else {
                throw Error();
            }
        } catch (error: any) {
            if (error?.response?.status === 400) {
                setResponseRequest({
                    status: 'warning',
                    message: error?.response?.data?.error?.message,
                });
            } else {
                setResponseRequest({
                    status: 'error',
                    message: 'Terjadi Kesalahan',
                });
            }
        }

        setLoading(false);
    };

    const onChangeTable: TableProps<DataType>['onChange'] = (pagination, filters, sorter, extra) => {
        _onFetchData(metadata?.pageSize, metadata?.page, sorter);
    };

    const onChange: PaginationProps['onChange'] = (page, pageSize) => {
        _onFetchData(pageSize, page);
    };

    const onShowSizeChange: PaginationProps['onShowSizeChange'] = (current, pageSize) => {
        _onFetchData(pageSize, current);
    };

    const onChangeDateRange = (_data: any, values: any) => { 
        setFilterDate(values);
    }

    const onSearch = (value: string) => { 
        setSearch(value);
    }

    useEffect(() => {
        _onFetchData();
    }, [search, filterDate]);

    useEffect(() => {
        checkExistLogin();
    }, [auth]);

    const _onLogOut = () => {
        dispatch(authClearData());
    }

    const columns: ColumnsType<DataType> = [
        {
            title: 'No.',
            dataIndex: 'no',
            key: 'no',
            render: (value, item, index) => ((metadata?.page - 1) * 10 + index) + 1 + '.',
        },
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            filterMode: 'tree',
            filterSearch: true,
            // onFilter: (value: string, record: any) => record.title.startsWith(value),
            sorter: (a: any, b: any) => a.title - b.title,
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: 'createdAt',
            sorter: (a: any, b: any) => a.createdAt - b.createdAt,
            render: (_, record: any) => (
                <Moment format="YYYY-MM-DD hh:mm:ss" date={record?.createdAt} />
            ),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (_, record: any) => (
                <Space size="middle">
                    Draft
                </Space>
            ),
        },
        {
            title: 'action',
            dataIndex: 'action',
            key: 'action',
            fixed: 'right',
            align: 'center',
            render: (_, record: any) => (
                <Space size="middle">
                    <Button 
                        onClick={() => { 
                            setDataItem(record); 
                            setForm(true); 
                        }} 
                        size="small"
                    >
                        Edit
                    </Button>
                    <Button 
                        danger 
                        onClick={() => { 
                            onDelete(record); 
                        }} 
                        size="small"
                    >
                        Hapus
                    </Button>
                </Space>
            ),
        },
    ];

    return (
        <>
        <Head />
        <div className="container">
            <div className="action-container mt-5 mb-3">
                <Row>
                    <Col span={12} className="text-left">
                        <Button 
                            icon={<LogoutOutlined />} 
                            onClick={_onLogOut} 
                            danger
                            className="mt-3 mr-3" 
                            loading={loading} 
                            disabled={loading}
                        >
                            Keluar
                        </Button>
                    </Col>
                    <Col span={12} className="text-right">
                        <Space>
                            <Button 
                                icon={<SyncOutlined />} 
                                onClick={() => { 
                                    // setDataItem({}); 
                                    // setForm(true); 
                                }} 
                                type="default" 
                                className="mt-3" 
                                loading={loading} 
                                disabled={loading}
                            >
                                Sinkron
                            </Button>
                            <Button 
                                icon={<PlusCircleOutlined />} 
                                onClick={() => { 
                                    setDataItem({}); 
                                    setForm(true); 
                                }} 
                                type="primary"
                                className="mt-3" 
                                loading={loading} 
                                disabled={loading}
                            >
                                Tambah
                            </Button>
                        </Space>
                    </Col>
                </Row>
            </div>
            <div className="article-container">
                <h2 className="mb-3 mt-3">
                    Halaman { form ? 'From' : 'Data' } Artikel
                </h2>
                {responseRequest?.status && (
                    <div className="mb-4">
                        <Alert
                            // message={showResponseLabel?.[responseRequest?.status]}
                            description={responseRequest?.message}
                            type={responseRequest?.status || 'info'}
                            showIcon
                            closable
                            onClose={() => setResponseRequest({
                                status: null,
                                massage: '',
                            })}
                        />
                    </div>
                )}
                {!form && (
                    <>
                        <div className="text-right mb-4">
                            <Space>
                                <Input.Group compact>
                                    <Input style={{ width: '30%' }} defaultValue="Filter Created At" />
                                    <DatePicker.RangePicker style={{ width: '70%' }} onChange={onChangeDateRange} />
                                </Input.Group>
                                <Search 
                                    placeholder="Cari Title" 
                                    allowClear 
                                    onSearch={onSearch} 
                                    enterButton={false} 
                                    style={{ maxWidth: 200 }} 
                                />
                            </Space>
                        </div>
                        <Table 
                            loading={loading}
                            dataSource={data} 
                            columns={columns} 
                            pagination={false}
                            onChange={onChangeTable}
                        />
                        <div className="text-right mt-4">
                            <Pagination
                                onChange={onChange}
                                showSizeChanger
                                onShowSizeChange={onShowSizeChange}
                                defaultCurrent={metadata?.page}
                                total={metadata?.total || 10}
                            />
                        </div>
                    </>
                )}
                {form && (
                    <Form 
                        loading={loading}
                        data={dataItem}
                        onSave={onSave}
                        onCancel={() => setForm(false)}
                    />
                )}
            </div>
        </div>
        </>
    );
};

export default Article;