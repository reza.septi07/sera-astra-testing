import React from 'react';
import {
    Button,
    Form,
    Input,
} from 'antd';
import PropTypes from 'prop-types';
import { SaveOutlined, RollbackOutlined } from '@ant-design/icons'

const ArticleForm = (props: any) => {
    const {
        loading, data, onSave, onCancel
    } = props;

    console.log('haiii', data);

    const [form] = Form.useForm();
  
    const onFinish = async (values: any) => {
        onSave(values, data?.id);
    };

    return (
        <div className="article-form-container">
            <Form
                layout="vertical"
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                    title: data?.title || '',
                    content: data?.content || '',
                }}
                scrollToFirstError
            >
                <Form.Item
                    name="title"
                    label="Title"
                    rules={[
                        {
                            required: true,
                            message: 'Title wajib diisi!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input style={{ maxWidth: 350 }}   />
                </Form.Item>
                
                <Form.Item
                    name="content"
                    label="Konten"
                    rules={[
                        {
                            required: true,
                            message: 'Konten wajib diisi!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input style={{ maxWidth: 350 }} />
                </Form.Item>

                <Form.Item>
                    <Button 
                        icon={<RollbackOutlined />} 
                        onClick={() => onCancel()} 
                        type="default" 
                        className="mt-3 mr-3" 
                        loading={loading} 
                        disabled={loading}
                    >
                        Kembali
                    </Button>
                    <Button 
                        icon={<SaveOutlined />} 
                        type="primary" 
                        htmlType="submit" 
                        className="mt-3" 
                        loading={loading} 
                        disabled={loading}
                    >
                        Simpan
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

ArticleForm.propTypes = {
    loading: PropTypes.bool,
    data: PropTypes.objectOf(PropTypes.any),
    onSave: PropTypes.func,
    onCancel: PropTypes.func,
};
  
ArticleForm.defaultProps = {
    loading: false,
    data: {},
    onSave: () => {},
    onCancel: () => {},
};


export default ArticleForm;