import {
    AUTH_START,
    AUTH_SUCCESS, 
    AUTH_ERROR,
    AUTH_LOGOUT,
} from '../types'

const initialState = {
    data: {},
    loading: false
}

export default function(state:any = initialState, action:any){

    switch(action.type){
        case AUTH_START:
            return{
                ...state,
                loading:true,
            }
        case AUTH_SUCCESS:
            return {
                ...state,
                data:action.payload,
                loading:false
            }
        case AUTH_ERROR:
            return{
                loading: false, 
                error: action.payload 
            }
        case AUTH_LOGOUT:
            return{
                loading: false, 
                data: {}, 
            }
        default: return state
    }

}