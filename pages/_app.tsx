import type { AppProps } from 'next/app'
import { store, persistor } from "../store/store";
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'
import { DBConfig } from '../DBConfig';
import { initDB } from 'react-indexed-db';
// import { register, unregister } from 'next-offline/runtime';

import 'antd/dist/antd.css'
import '../styles/vars.css'
import '../styles/global.css'
import '../styles/login.css';
import '../styles/register.css';
import '../styles/article.css';

// initDB(DBConfig);

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Component {...pageProps} />
        </PersistGate>
      </Provider>
    </>
  );
}

export default MyApp;
