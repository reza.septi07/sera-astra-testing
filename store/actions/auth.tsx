import axios from 'axios';
import {
    AUTH_START,
    AUTH_SUCCESS, 
    AUTH_ERROR,
    AUTH_LOGOUT,
} from '../types'

export function authStart() {
  return {
    type: AUTH_START,
  };
}

export function authError(payload:any) {
  return {
    type: AUTH_ERROR,
    payload,
  };
}

export function authSuccess(payload:any) {
  return {
    type: AUTH_SUCCESS,
    payload,
  };
}

export function authClearData() {
  return {
    type: AUTH_LOGOUT,
  };
}

export function authenticate(values:any, onSuccess:Function, onError:Function) {
  return async (dispatch:any) => {
    const reg = /^\d+$/;
    dispatch(authStart());
    try {
        const url = 'https://fe-technical-test.herokuapp.com/api/auth/local';
        const response: any = await axios.post(url, {
            identifier: values?.username,
            password: values?.password,
        });

        if (response?.status === 200) {
            const resp = {
                ...response?.data?.user,
                token: response?.data?.jwt,
            };
            
            dispatch(authSuccess(resp));
            onSuccess();
        } else {
            throw Error();
        }
    } catch (err: any) {

      let _error: any = {
        status: 'error',
        message: 'Terjadi Kesalahan',
      };

      if (err?.response?.status === 400) {
          _error = {
            status: 'warning',
            // message: err?.response?.data?.error?.message,
            message: 'Username atau password salah',
          };
      } 

      dispatch(authError(err?.error));
      onError(_error);
    }
  };
}

// export function logout(token, onSuccess, onError) {
//     return async (dispatch) => {
//       dispatch(authStart());
//       try {
//         const url = API_ENDPOINTS.logout;
//         const headers = {
//           Authorization: `Bearer ${token}`,
//         };
//         const request = await ApiRequest.post(url, null, headers);
//         if (request && request.status === 200) {
//           dispatch({
//             type: LOGOUT,
//           });
//           const st = setTimeout(() => {
//             onSuccess();
//             clearTimeout(st);
//           }, 500);
//         } else {
//           const response = await request.json();
//           throw response.error;
//         }
//       } catch (err) {
//         // The token is probably expired.
//         // Force to clear the session.
//         if (err?.errors?.code === 401) {
//           dispatch({
//             type: LOGOUT,
//           });
//           const st = setTimeout(() => {
//             onSuccess();
//             clearTimeout(st);
//           }, 250);
//         } else {
//           const _errorMessage = err?.errors?.message || 'Sorry, we encountered an error.';
//           dispatch(authError(_errorMessage));
//           onError(_errorMessage);
//         }
//       }
//     };
//   }