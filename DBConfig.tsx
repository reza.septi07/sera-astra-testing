export const DBConfig = {
    name: 'MyDB',
    version: 1,
    objectStoresMeta: [
      {
        store: 'article',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'attributes', keypath: 'attributes', options: { unique: false } },
        ]
      }
    ]
  };