import React, { useState } from 'react';
import {
    Button,
    Form,
    Input,
    Alert,
} from 'antd';
import axios from 'axios';
import Link from 'next/link';
import { SaveOutlined, RollbackOutlined } from '@ant-design/icons'
import { REGEX_RULE } from '../../helpers/constants';

// const { Option } = Select;

const formItemLayout:any = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
};

const tailFormItemLayout:any = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
};

const defaultResponse:any = {
    status: null,
    massage: '',
}

// const showResponseLabel:any = {
//     success: 'Berhasil',
//     error: 'Gagal',
//     warning: 'Peringatan',
// }

const RegisterForm = () => {
    const [form] = Form.useForm();
    const [loading, setLoading] = useState<boolean>(false);
    const [responseSubmit, setResponseSubmit] = useState<any>(defaultResponse);
  
    const onFinish = async (values: any) => {
        setLoading(true);

        try {
            const url = 'https://fe-technical-test.herokuapp.com/api/auth/local/register';
            const response = await axios.post(url, {
              username: values?.username,
              email: values?.email,
              password: values?.password,
            });

            if (response?.status === 200) {
                setResponseSubmit({
                    status: 'success',
                    message: 'Berhasil mendaftar',
                });
            } else {
                throw Error();
            }
        } catch (error: any) {
            if (error?.response?.status === 400) {
                setResponseSubmit({
                    status: 'warning',
                    message: error?.response?.data?.error?.message,
                });
            } else {
                setResponseSubmit({
                    status: 'error',
                    message: 'Terjadi Kesalahan',
                });
            }
        }

        setLoading(false);
    };

    return (
        <div className="container">
            <div className="register-container mt-5">
                <h2 className="mb-3 mt-3 text-center">
                    Halaman Pendaftaran
                </h2>
                {responseSubmit?.status && (
                    <div className="mb-4">
                        <Alert
                            // message={showResponseLabel?.[responseSubmit?.status]}
                            description={responseSubmit?.message}
                            type={responseSubmit?.status || 'info'}
                            showIcon
                            closable
                        />
                    </div>
                )}
                <Form
                        {...formItemLayout}
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        initialValues={{}}
                        scrollToFirstError
                    >
                        <Form.Item
                            name="username"
                            label="Username"
                            tooltip="Isikan nama singkat kamu untuk kebutuhan login !"
                            rules={[
                                {
                                    required: true,
                                    message: 'Username wajib diisi!',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        
                        <Form.Item
                            name="email"
                            label="E-mail"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'Email belum sesuai format!',
                                },
                                {
                                    required: true,
                                    message: 'Email wajib diisi!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            label="Password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Password wajib diisi!',
                                },
                                {
                                    min: 8,
                                    message: 'Minimal 8 Karakter!',
                                },
                                () => ({
                                    validator(_, value) {
                                        if (new RegExp(REGEX_RULE.password).test(value) || value.length < 8) {
                                            return Promise.resolve();
                                        }

                                        return Promise.reject(new Error('Format harus terdiri dari huruf besar, huruf kecil, angka, dan simbol'));
                                    },
                                }),
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            label="Konfirmasi Password"
                            dependencies={['password']}
                            hasFeedback
                            rules={[
                                {
                                    required: true,
                                    message: 'Konfirmasi Password wajib diisi!',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }

                                        return Promise.reject(new Error('Konfirmasi password tidak cocok!'));
                                    },
                                }),
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item {...tailFormItemLayout}>
                            <Link href="/login">
                                <Button icon={<RollbackOutlined />} type="default" className="mt-3 mr-3" loading={loading} disabled={loading}>
                                    Kembali
                                </Button>
                            </Link>
                            <Button icon={<SaveOutlined />} type="primary" htmlType="submit" className="mt-3" loading={loading} disabled={loading}>
                                Daftar
                            </Button>
                        </Form.Item>
                </Form>
            </div>
        </div>
    );
  };

export default RegisterForm;