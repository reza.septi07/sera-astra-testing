export const AUTH_START: String = 'AUTH_START';
export const AUTH_SUCCESS: String = 'AUTH_SUCCESS';
export const AUTH_ERROR: String = 'AUTH_ERROR';
export const AUTH_LOGOUT: String = 'AUTH_LOGOUT';